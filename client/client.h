//
// Created by Артур Кулапин on 24.04.2021.
//

#ifndef MINI_FILE_SYSTEM_CLIENT_CLIENT_H_
#define MINI_FILE_SYSTEM_CLIENT_CLIENT_H_

#include "../utils/defines.h"

int ClientLoop(const char* address);

#endif //MINI_FILE_SYSTEM_CLIENT_CLIENT_H_
