//
// Created by Артур Кулапин on 24.04.2021.
//

#ifndef MINI_FILE_SYSTEM_CLIENT_HANDLERS_H_
#define MINI_FILE_SYSTEM_CLIENT_HANDLERS_H_

#include <stdbool.h>

bool ReceiveResult(int socket);

bool SendExitCommand(int socket);
bool SendInitCommand(int socket);
bool SendLsCommand(int socket, const char* path);
bool SendMkdirCommand(int socket, const char* path);
bool SendTouchCommand(int socket, const char* path);
bool SendOpenCommand(int socket, const char* path);
bool SendCloseCommand(int socket, const char* path);
bool SendWriteCommand(int socket);
bool SendReadCommand(int socket);
bool SendLseekCommand(int socket);



#endif //MINI_FILE_SYSTEM_CLIENT_HANDLERS_H_
