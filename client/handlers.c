//
// Created by Артур Кулапин on 24.04.2021.
//

#include "handlers.h"
#include "../utils/net_utils.h"

#include <stdlib.h>
#include <unistd.h>
#include <string.h>

bool ReceiveResult(int socket) {
  size_t size = 0;
  char* data = NULL;
  if (!ReceiveData(socket, &data, &size)) {
    if (data != NULL) {
      free(data);
    }

    return false;
  }

  if (data == NULL) {
    return false;
  }

  if (WriteWhile(STDOUT_FILENO, data, size) == -1) {
    free(data);
    return false;
  }

  free(data);
  return true;
}

bool SendInitCommand(int socket) {
  return SendCommand(socket, Init);
}

bool SendLsCommand(int socket, const char* path) {
  if (!SendCommand(socket, Ls) || !SendData(socket, path, strlen(path))) {
    return false;
  }
  return ReceiveResult(socket);
}

bool SendMkdirCommand(int socket, const char* path) {
  SendCommand(socket, Mkdir);
  SendData(socket, path, strlen(path));
  ReceiveResult(socket);
}

bool SendTouchCommand(int socket, const char* path) {
  SendCommand(socket, Touch);
  SendData(socket, path, strlen(path));
  ReceiveResult(socket);
}

bool SendOpenCommand(int socket, const char* path) {
  SendCommand(socket, Open);
  SendData(socket, path, strlen(path));
  ReceiveResult(socket);
}

bool SendCloseCommand(int socket, const char* path) {
  SendCommand(socket, Close);
  SendData(socket, path, strlen(path));
  ReceiveResult(socket);
}

bool SendWriteCommand(int socket) {
  SendCommand(socket, Write);
  ReceiveResult(socket);
}

bool SendReadCommand(int socket) {
  SendCommand(socket, Read);
  ReceiveResult(socket);
}

bool SendLseekCommand(int socket) {
  SendCommand(socket, LSeek);
  ReceiveResult(socket);
}

bool SendExitCommand(int socket) {
  SendCommand(socket, Exit);
}