//
// Created by Артур Кулапин on 24.04.2021.
//

#include "client.h"
#include "../utils/utils.h"
#include "../utils/net_utils.h"
#include "handlers.h"

#include <string.h>
#include <stdio.h>

char* ParseCommand(char* command_buffer, char* command) {
  char* first_space = strchr(command_buffer, ' ');

  if (first_space == NULL) {
    strcpy(command, command_buffer);
  } else {
    uint16_t command_length = first_space - command_buffer;
    memcpy(command, command_buffer, command_length);
    command[command_length] = '\0';
  }

  return first_space == NULL ? NULL : first_space + 1;
}

int ClientLoop(const char* address) {
  int sock = MakeConnection(address);
  char buffer[MAX_COMMAND_LENGTH];
  printf("Connection made. socket: %d\n", sock);
  while (true) {
    ReadCommand(buffer, MAX_COMMAND_LENGTH);
    char command[MAX_COMMAND_LENGTH];
    char* first_arg_pos = ParseCommand(buffer, command);

    if (strcmp("exit", command) == 0) {
      if (!SendExitCommand(sock)) {
        fprintf(stderr, "Command is not delivered - try again\n");
      }
      break;
    } else if (strcmp("init", command) == 0) {
      if (!SendInitCommand(sock)) {
        fprintf(stderr, "Command is not delivered - try again\n");
        SendExitCommand(sock);
        break;
      }
    } else if (strcmp("ls", command) == 0) {
      if (first_arg_pos == NULL || strlen(first_arg_pos) == 0) {
        printf("Ls requires path\n");
        continue;
      }
      char path[MAX_COMMAND_LENGTH];
      ParseCommand(first_arg_pos, path);
      if (!SendLsCommand(sock, path)) {
        fprintf(stderr, "Command is not delivered - try again\n");
        SendExitCommand(sock);
        break;
      }
    } else if (strcmp("mkdir", command) == 0) {
      if (first_arg_pos == NULL || strlen(first_arg_pos) == 0) {
        printf("mkdir requires path\n");
        continue;
      }
      char path[MAX_COMMAND_LENGTH];
      ParseCommand(first_arg_pos, path);
      if (!SendMkdirCommand(sock, path)) {
        fprintf(stderr, "Command is not delivered - try again\n");
        SendExitCommand(sock);
        break;
      }
    } else if (strcmp("touch", command) == 0) {
      if (first_arg_pos == NULL || strlen(first_arg_pos) == 0) {
        printf("touch requires path\n");
        continue;
      }
      char path[MAX_COMMAND_LENGTH];
      ParseCommand(first_arg_pos, path);
      if (!SendTouchCommand(sock, path)) {
        fprintf(stderr, "Command is not delivered - try again\n");
        SendExitCommand(sock);
        break;
      }
    } else if (strcmp("open", command) == 0) {
      if (first_arg_pos == NULL || strlen(first_arg_pos) == 0) {
        printf("open requires path\n");
        continue;
      }
      char path[MAX_COMMAND_LENGTH];
      ParseCommand(first_arg_pos, path);
      if (!SendOpenCommand(sock, path)) {
        fprintf(stderr, "Command is not delivered - try again\n");
        SendExitCommand(sock);
        break;
      }
    } else if (strcmp("close", command) == 0) {
      if (first_arg_pos == NULL || strlen(first_arg_pos) == 0) {
        printf("close requires path\n");
        continue;
      }
      char path[MAX_COMMAND_LENGTH];
      ParseCommand(first_arg_pos, path);
      if (!SendCloseCommand(sock, path)) {
        fprintf(stderr, "Command is not delivered - try again\n");
        SendExitCommand(sock);
        break;
      }
    } else {
      printf("Unknown command\n");
      continue;
    }
  }

  CloseConnection(sock);
  return 0;
}



