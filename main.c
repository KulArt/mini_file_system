#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>

#include "client/client.h"

int main(int argc, char** argv) {
  if (argc < 2) {
    fprintf(stderr, "Path to fs file wasn't specified. Using default name!\n");
    const char* fs_file_path = "127.0.0.1:3000";
    ClientLoop(fs_file_path);
  } else {
    const char* fs_file_path = argv[1];
    ClientLoop(fs_file_path);
  }
}

