# Mini file system

It is ext like file system with interface below:

* `init` - inits session of interaction with file system

* `ls *path*` - prints all files and directories in *path*, actually, root directory is / and all paths are relative here and below

* `mkdir *path_to_dir*` - creates directory with path argument

* `touch *path_to_file*` - creates directory with path argument

* `open *path_to_file*` - opens file with such path, prints you file descriptor (*Attention: there are only 10 ones*)

* `close *file_descriptor*` - closes file descriptor

* `write *fd*, *size*, *data*` - writes to file with open descriptor *fd* *size* bytes from *data*

* `read *fd* *size* *data*` - reads to *data*, prints *size* bytes of file from *fd*

* `lseek *fd* *pos*` - moves *fd* to *pos* position in *fd* responsile file for

* `readto *fd* *size* *path*` - reads from *fd*  *size* bytes and write it to absolute *path* in your file system

* `writefrom *fd* *path*` - write all content of *path* file to *fd* file in mini fs

* `exit` - stops interaction with mini fs
