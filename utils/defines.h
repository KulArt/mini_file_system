//
// Created by Артур Кулапин on 11.03.2021.
//

#ifndef MINI_FILE_SYSTEM_SRC_DEFINES_H_
#define MINI_FILE_SYSTEM_SRC_DEFINES_H_


#define BLOCK_SIZE 128
#define INODE_COUNT 128
#define BLOCK_COUNT 128
#define NUM_BLOCK_IN_INODE 128
#define DIR_NAME_LENGTH 10
#define NUM_DESCRIPTORS 10

#define SUPERBLOCK_SIZE (sizeof(struct SuperBlock))
#define INODE_SIZE (sizeof(struct Inode))
#define BLOCK_RECORD_SIZE (sizeof(struct BlockRecord))
#define DESCRIPTOR_TABLE_SIZE (sizeof(struct DescriptorTable))

#define FS_SIZE                  \
    (SUPERBLOCK_SIZE +           \
    DESCRIPTOR_TABLE_SIZE +      \
    INODE_SIZE * INODE_COUNT +    \
    BLOCK_SIZE * BLOCK_COUNT)    \

#define MAX_COMMAND_LENGTH 256

#endif //MINI_FILE_SYSTEM_SRC_DEFINES_H_
