//
// Created by Артур Кулапин on 24.04.2021.
//

#ifndef MINI_FILE_SYSTEM_NET_UTILS_NET_UTILS_H_
#define MINI_FILE_SYSTEM_NET_UTILS_NET_UTILS_H_

#include "commands.h"
#include "utils.h"

#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>


void ParseAddress(const char* address, char** host, long* port);

int MakeConnection(const char* address);
void CloseConnection(int socket);


bool SendCommand(int socket, enum Command command);
bool SendData(int socket, char* data, size_t size);

bool ReceiveCommand(int socket, enum Command* command);
bool ReceiveData(int socket, char** data, size_t* size);

#endif //MINI_FILE_SYSTEM_NET_UTILS_NET_UTILS_H_
