//
// Created by Артур Кулапин on 11.03.2021.
//

#ifndef MINI_FILE_SYSTEM_SRC_UTILS_H_
#define MINI_FILE_SYSTEM_SRC_UTILS_H_

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>


int ReadWhile(int fd, char* buffer, size_t to_read);
int WriteWhile(int fd, const char* buffer, size_t to_write);

int FindFirstNonZero(const bool* arr, int len);

void ReadCommand(char* command_buffer, size_t buffer_size);

#endif //MINI_FILE_SYSTEM_SRC_UTILS_H_
