//
// Created by Артур Кулапин on 11.03.2021.
//

#include "utils.h"

int ReadWhile(const int fd, char* buffer, size_t to_read) {
  size_t total_read = 0;
  while (total_read != to_read) {
    ssize_t current_read = read(fd, buffer + total_read,
                                    to_read - total_read);
    if (current_read == -1) {
      return current_read;
    }
    total_read += current_read;
  }
  return total_read;
}

int WriteWhile(const int fd, const char* buffer, size_t to_write) {
  size_t total_written = 0;
  while (total_written != to_write) {
    ssize_t current_written = write(fd, buffer + total_written,
                                    to_write - total_written);
    if (current_written == -1) {
      return current_written;
    }
    total_written += current_written;
  }
  return total_written;
}

int FindFirstNonZero(const bool* arr, int len) {
  for (int i = 0; i < len; ++i) {
    if (!arr[i]) {
      return i;
    }
  }
  return -1;
}

void ReadCommand(char* command_buffer, size_t buffer_size) {
  fgets(command_buffer, buffer_size, stdin);
  uint32_t lenght = strlen(command_buffer);
  command_buffer[lenght - 1] = '\0';
}
