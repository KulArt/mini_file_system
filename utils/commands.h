//
// Created by Артур Кулапин on 24.04.2021.
//

#ifndef MINI_FILE_SYSTEM_NET_UTILS_COMMANDS_H_
#define MINI_FILE_SYSTEM_NET_UTILS_COMMANDS_H_

#include <stdint.h>

enum Command: uint8_t {
  Ls = 1,
  Init,
  Mkdir,
  Touch,
  Open,
  Exit,
  Close,
  Write,
  Read,
  LSeek
};

#endif //MINI_FILE_SYSTEM_NET_UTILS_COMMANDS_H_
