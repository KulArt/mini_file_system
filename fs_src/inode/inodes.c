//
// Created by Артур Кулапин on 11.03.2021.
//

#include "inodes.h"

void BuildInode(Inode* inode, int16_t id, bool is_file) {
  inode->is_file = is_file;
  inode->id = id;
  inode->taken_blocks_count = 0;
  for (int i = 0; i < NUM_BLOCK_IN_INODE; ++i) {
    inode->block_ids[i] = 129;
  }
}

void FreeInode(Inode* inode) {
  for (int i = 0; i < NUM_BLOCK_IN_INODE; ++i) {
    inode->block_ids[i] = 129;
  }
  inode->id = -1;
  inode->is_file = false;
}