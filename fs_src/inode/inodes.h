//
// Created by Артур Кулапин on 11.03.2021.
//

#ifndef MINI_FILE_SYSTEM_SRC_INODES_H_
#define MINI_FILE_SYSTEM_SRC_INODES_H_

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "../../utils/defines.h"


typedef struct Inode {
  uint16_t block_ids[NUM_BLOCK_IN_INODE];
  int16_t id;
  bool is_file;
  uint16_t taken_blocks_count;
} Inode;

void BuildInode(Inode* inode, int16_t id, bool is_file);
void FreeInode(Inode* inode);

#endif //MINI_FILE_SYSTEM_SRC_INODES_H_
