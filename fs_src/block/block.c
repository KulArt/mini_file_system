//
// Created by Артур Кулапин on 11.03.2021.
//

#include "block.h"

void BuildBlock(Block* block, int id, int inode_parent_id, SuperBlock* super_block) {
  block->id = id;
  block->inode_parent_id = inode_parent_id;
  block->records_count = 0;
  block->block_records = NULL;
  block->data = NULL;
  block->data_size = 0;
  if (super_block->taken_inodes_mask[inode_parent_id]) {
    fprintf(stderr, "%s %d is taken", "inode with id: ", id);
    exit(EXIT_FAILURE);
  }
  super_block->taken_blocks_mask[inode_parent_id] = true;
  super_block->taken_inodes_mask[inode_parent_id] = true;
  ++super_block->taken_blocks_count;
}

void DestroyBlock(Block* block, SuperBlock* super_block) {
  if (block->data != NULL) {
    free(block->data);
  }
  for (int i = 0; i < block->records_count; ++i) {
    DestroyBlockRecord(block->block_records + i);
  }
  block->block_records = NULL;
  block->data = NULL;
  block->inode_parent_id = -1;
  block->id = -1;
  block->records_count = 0;
  block->data_size = 0;
  if (super_block->taken_blocks_mask[block->id] == false) {
    fprintf(stderr, "%s with id: %d", "can't double free block", block->id);
    exit(EXIT_FAILURE);
  }
  super_block->taken_blocks_mask[block->id] = false;
  --super_block->taken_blocks_count;

  if (super_block->taken_inodes_mask[block->inode_parent_id] == false) {
    fprintf(stderr, "%s with id: %d", "can't double free inode", block->inode_parent_id);
    exit(EXIT_FAILURE);
  }
  super_block->taken_inodes_mask[block->inode_parent_id] = false;
}


ssize_t ReadBlock(int fd, Block* block, int id, SuperBlock* super_block) {
  ssize_t offset = SUPERBLOCK_SIZE + DESCRIPTOR_TABLE_SIZE + INODE_COUNT * sizeof(Inode) + BLOCK_SIZE * id;
  lseek(fd, offset, SEEK_SET);
  int meta[3];
  ssize_t read_meta = ReadWhile(fd, (char*)meta, 3 * sizeof(int));
  if (read_meta == -1) {
    fprintf(stderr, "%s", "can't read block meta");
    exit(EXIT_FAILURE);
  }
  offset += read_meta;
  lseek(fd, offset, SEEK_SET);
  block->id = meta[0];
  block->inode_parent_id = meta[1];
  block->records_count = meta[2];
  block->block_records = (BlockRecord*)calloc(block->records_count, sizeof(BlockRecord));
  block->data = (char*)calloc(block->data_size, sizeof(char));
  offset += read_meta;
  if (block->records_count >= 2) {
    ssize_t read_block_records = ReadWhile(fd, (char*)block->block_records,
                                           block->records_count * sizeof(BlockRecord));
    if (read_block_records == -1) {
      DestroyBlock(block, super_block);
      fprintf(stderr, "%s", "can't read block records");
      exit(EXIT_FAILURE);
    }
    offset += read_block_records;
  } else {
    ssize_t read_data = ReadWhile(fd, block->data, block->data_size);
    if (read_data == -1) {
      DestroyBlock(block, super_block);
      fprintf(stderr, "%s", "can't read block data");
      exit(EXIT_FAILURE);
    }
    offset += read_data;
  }

  return offset;
}

ssize_t WriteBlock(int fd, Block* block, SuperBlock* super_block) {
  ssize_t offset = SUPERBLOCK_SIZE + DESCRIPTOR_TABLE_SIZE + INODE_COUNT * sizeof(Inode) + BLOCK_SIZE * block->id;
  lseek(fd, offset, SEEK_SET);
  int meta[3] = {block->id, block->inode_parent_id, block->records_count};
  ssize_t written_meta = WriteWhile(fd, (char*)meta, 3 * sizeof(int));
  if (written_meta == -1) {
    fprintf(stderr, "%s", "can't write block meta");
    exit(EXIT_FAILURE);
  }
  offset += written_meta;
  lseek(fd, offset, SEEK_SET);
  if (block->records_count >= 2) {
    //it is directory
    ssize_t written_block_records = WriteWhile(fd,
                                               (char*)block->block_records,
                                               block->records_count * sizeof(BlockRecord));
    if (written_block_records == -1) {
      fprintf(stderr, "%s", "can't write block records");
      exit(EXIT_FAILURE);
    }
    offset += written_block_records;
  } else {
    //it is a file
    ssize_t written_data = WriteWhile(fd, block->data, block->data_size);
    if (written_data == -1) {
      fprintf(stderr, "%s", "can't write block data");
      exit(EXIT_FAILURE);
    }
    offset += written_data;
  }
  super_block->taken_blocks_mask[block->id] = true;
  return offset;
}