//
// Created by Артур Кулапин on 11.03.2021.
//

#ifndef MINI_FILE_SYSTEM_SRC_BLOCK_BLOCK_H_
#define MINI_FILE_SYSTEM_SRC_BLOCK_BLOCK_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "../inode/inodes.h"
#include "../blockrecord/blockrecord.h"
#include "../../utils/defines.h"
#include "../superblock/superblock.h"


typedef struct Block {
  int id;
  int inode_parent_id;
  int records_count;
  BlockRecord* block_records;
  int data_size;
  char* data;
} Block;

void BuildBlock(Block* block, int id, int inode_parent_id, SuperBlock* super_block);
void BuildBlockWithData(Block* block, int id, int inode_parent_id, char* data, int data_len);
void BuildBlockWithRecords(Block* block, int id, int inode_parent_id, int records_count);
void DestroyBlock(Block* block, SuperBlock* super_block);

ssize_t ReadBlock(int fd, Block* block, int id, SuperBlock* super_block);
ssize_t WriteBlock(int fd, Block* block, SuperBlock* super_block);


#endif //MINI_FILE_SYSTEM_SRC_BLOCK_BLOCK_H_
