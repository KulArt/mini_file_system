//
// Created by Артур Кулапин on 12.03.2021.
//

#ifndef MINI_FILE_SYSTEM_SRC_FILEUTILS_FILEUTILS_H_
#define MINI_FILE_SYSTEM_SRC_FILEUTILS_FILEUTILS_H_

#include "../superblock/superblock.h"
#include "../block/block.h"
#include "../inode/inodes.h"
#include "../blockrecord/blockrecord.h"

uint16_t CreateDirBlockAndInode(int fd, bool is_root, uint16_t prev_inode_id, SuperBlock* sb);

#endif //MINI_FILE_SYSTEM_SRC_FILEUTILS_FILEUTILS_H_
