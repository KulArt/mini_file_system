//
// Created by Артур Кулапин on 12.03.2021.
//

#include "fileutils.h"

uint16_t CreateDirBlockAndInode(int fd, bool is_root, uint16_t prev_inode_id, SuperBlock* sb) {
  int inode_id = FindFirstNonZero(sb->taken_inodes_mask, NUM_BLOCK_IN_INODE);

  Inode inode;
  BuildInode(&inode, inode_id, false);
  inode.taken_blocks_count = 1;
  inode.id = inode_id;

  Block block;
  BuildBlock(&block, sb->taken_blocks_count, inode_id, sb);
  block.block_records = (BlockRecord*)calloc(2, sizeof(BlockRecord));
  block.inode_parent_id = 0;

  // create . and ..
  DestroyBlockRecord(&block.block_records[0]);
  DestroyBlockRecord(&block.block_records[1]);
  block.block_records[0].inode_parent_id = inode_id;
  block.block_records[1].inode_parent_id = is_root ? inode_id : prev_inode_id;
  block.records_count += 2;
  block.data_size += sizeof(char) * 3;
  strcpy(block.block_records[0].name, ".");
  strcpy(block.block_records[1].name, "..");

  inode.block_ids[0] = block.id;

  WriteSuperBlock(fd, sb);
  WriteInode(fd, &inode);
  WriteBlock(fd, &block, sb);
  ReadBlock(fd, &block, block.id, sb);
  return inode_id;
}