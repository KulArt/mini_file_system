//
// Created by Артур Кулапин on 12.03.2021.
//

#include <string.h>

#include "stringhelper.h"

void TraverseRecursively(int fd, const char* path, Inode* current, Inode* res, SuperBlock* sb) {
  if (current->is_file) {
    fprintf(stderr, "try to enter file");
    exit(EXIT_FAILURE);
  }

  char current_layer[256];
  char* remaining_path = ParsePath(path, current_layer);

  if (strcmp(current_layer, "/") == 0) {
    memcpy(res, current, sizeof(Inode));
  } else {
    Block block;
    ReadBlock(fd, &block, current->block_ids[0], sb);
    bool dir_found = false;
    uint16_t i = 0;
    for (; i < block.records_count; ++i) {
      if (strcmp(block.block_records[i].name, current_layer) == 0) {
        dir_found = true;
        break;
      }
    }

    if (!dir_found) {
      fprintf(stderr, "no such directory");
      exit(EXIT_FAILURE);
    }

    Inode next;
    next.id = block.block_records[i].inode_parent_id;
    ReadInode(fd, &next);

    if (next.is_file) {
      fprintf(stderr, "try to enter file");
      exit(EXIT_FAILURE);
    }

    if (remaining_path == NULL) {
      memcpy(res, &next, sizeof(Inode));
    } else {
      TraverseRecursively(fd, remaining_path, &next, res, sb);
    }
  }
}

void TraversePath(int fd, const char* path, Inode* res, SuperBlock* sb) {
  Inode cur_inode;
  cur_inode.id = 0;
  ReadInode(fd, &cur_inode);
  TraverseRecursively(fd, path, &cur_inode, res, sb);
}

char* ParsePath(const char* path, char* next_token) {
  char* slash_pos = strchr(path, '/');
  if (slash_pos == NULL) {
    fprintf(stderr, "no / in path");
    exit(EXIT_FAILURE);
  }
  uint16_t path_length = strlen(path);
  if (path_length == 1) {
    strcpy(next_token, "/");
    return NULL;
  }

  char* next_slash_pos = strchr(slash_pos + 1, '/');
  uint16_t next_token_length = 0;
  if (next_slash_pos == NULL) {
    next_token_length = path + path_length - slash_pos - 1;
  } else {
    next_token_length = next_slash_pos - slash_pos - 1;

    if (next_token_length == 0) {
      fprintf(stderr, "incorrect path");
      exit(EXIT_FAILURE);
    }
  }
  memcpy(next_token, slash_pos + 1, next_token_length);
  next_token[next_token_length] = '\0';

  return next_slash_pos;
}

void DeleteLastSlashAndCopyRes(const char* str, uint16_t str_length, char* dest) {
  memcpy(dest, str, str_length);
  if (*(str + str_length - 1) != '/') {
    dest[str_length] = '\0';
  } else {
    dest[str_length - 1] = '\0';
  }
}

int32_t FindLastChar(const char* str, char ch) {
  uint16_t length = strlen(str);
  for (int i = length - 1; i >= 0; --i) {
    if (*(str + i) == ch) {
      return i;
    }
  }
  return -1;
}

void SplitPath(const char* path, char* path_to_traverse, char* dir_name) {
  uint16_t path_length = strlen(path);
  char buffer[256];
  DeleteLastSlashAndCopyRes(path, path_length, buffer);

  uint16_t buffer_length = strlen(buffer);
  if (path[0] != '/' || buffer_length == 0) {
    fprintf(stderr, "incorrect path");
    exit(EXIT_FAILURE);
  }

  int32_t last_slash_pos = FindLastChar(buffer, '/');
  memcpy(dir_name,
         buffer + last_slash_pos + 1,
         buffer_length - last_slash_pos - 1);
  dir_name[buffer_length - last_slash_pos - 1] = '\0';

  if (strlen(dir_name) == 0) {
    fprintf(stderr, "incorrect path");
    exit(EXIT_FAILURE);
  }

  memcpy(path_to_traverse, buffer, last_slash_pos + 1);
  path_to_traverse[last_slash_pos + 1] = '\0';
}

