//
// Created by Артур Кулапин on 12.03.2021.
//

#ifndef MINI_FILE_SYSTEM_SRC_STRINGHELPER_STRINGHELPER_H_
#define MINI_FILE_SYSTEM_SRC_STRINGHELPER_STRINGHELPER_H_

#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

#include "../inode/inodes.h"
#include "../block/block.h"

void TraversePath(int fd, const char* path, Inode* res, SuperBlock* sb);

char* ParsePath(const char* path, char* next_token);

void SplitPath(const char* path, char* path_to_traverse, char* dir_name);

#endif //MINI_FILE_SYSTEM_SRC_STRINGHELPER_STRINGHELPER_H_
