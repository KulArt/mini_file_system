//
// Created by Артур Кулапин on 12.03.2021.
//

#include "descriptortable.h"


uint16_t BuildDescriptor(DescriptorTable* dt, uint16_t inode_id) {
  for (uint16_t i = 0; i < NUM_DESCRIPTORS; ++i) {
    if (dt->inode_id[i] == inode_id) {
      fprintf(stderr, "%s %d is taken", "inode with id:", inode_id);
      exit(EXIT_FAILURE);
    }
  }

  for (uint16_t i = 0; i < NUM_DESCRIPTORS; ++i) {
    if (dt->fd_mask[i] == false) {
      dt->fd_mask[i] = true;
      dt->inode_id[i] = inode_id;
      dt->pos[i] = 0;
      return i;
    }
  }
  fprintf(stderr, "no empty free descriptors");
  exit(EXIT_FAILURE);
}

void InitDescriptorTable(DescriptorTable* dt) {
  for (uint16_t i = 0; i < NUM_DESCRIPTORS; ++i) {
    dt->fd_mask[i] = false;
    dt->inode_id[i] = -1;
    dt->pos[i] = 0;
  }
}

void DestroyDescriptorTable(DescriptorTable* dt) {
  for (uint16_t i = 0; i < NUM_DESCRIPTORS; ++i) {
    dt->fd_mask[i] = false;
    dt->inode_id[i] = 0;
    dt->pos[i] = 0;
  }
}


void FreeDescriptor(DescriptorTable* dt, uint16_t id) {
  if (dt->fd_mask[id] == false) {
    fprintf(stderr, "try to double free file descriptor with id: %d", id);
    exit(EXIT_FAILURE);
  }
  dt->fd_mask[id] = false;
  dt->inode_id[id] = 0;
  dt->pos[id] = 0;
}

void ChangePos(DescriptorTable* dt, uint16_t id, uint32_t pos) {
  if (dt->fd_mask[id] == false) {
    fprintf(stderr, "trying to use closed file descriptor with id: %d", id);
    exit(EXIT_FAILURE);
  }
  dt->pos[id] = pos;
}

ssize_t ReadDescriptorTable(int fd, DescriptorTable* dt) {
  lseek(fd, SUPERBLOCK_SIZE, SEEK_SET);
  ssize_t total_read = ReadWhile(fd, (char*) dt, sizeof(struct DescriptorTable));
  if (total_read == -1) {
    fprintf(stderr, "%s", "can't read descriptor table");
    exit(EXIT_FAILURE);
  }
  return total_read;
}

ssize_t WriteDescriptorTable(int fd, DescriptorTable* dt) {
  lseek(fd, SUPERBLOCK_SIZE, SEEK_SET);
  ssize_t total_written =
      WriteWhile(fd, (const char*)dt, sizeof(DescriptorTable));
  if (total_written == -1) {
    fprintf(stderr, "%s", "can't write descriptor table");
    exit(EXIT_FAILURE);
  }
  return total_written;
}