//
// Created by Артур Кулапин on 12.03.2021.
//

#ifndef MINI_FILE_SYSTEM_SRC_DESCRIPTORTABLE_DESCRIPTORTABLE_H_
#define MINI_FILE_SYSTEM_SRC_DESCRIPTORTABLE_DESCRIPTORTABLE_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "../../utils/defines.h"
#include "../../utils/utils.h"
#include "../superblock/superblock.h"

typedef struct DescriptorTable {
  bool fd_mask[NUM_DESCRIPTORS];
  int16_t inode_id[NUM_DESCRIPTORS];
  uint32_t pos[NUM_DESCRIPTORS];
} DescriptorTable;


void InitDescriptorTable(DescriptorTable* dt);
void DestroyDescriptorTable(DescriptorTable* dt);

uint16_t BuildDescriptor(DescriptorTable* dt, uint16_t inode_id);
void FreeDescriptor(DescriptorTable* dt, uint16_t id);

void ChangePos(DescriptorTable* dt, uint16_t descr, uint32_t pos);

ssize_t ReadDescriptorTable(int fd, DescriptorTable* dt);
ssize_t WriteDescriptorTable(int fd, DescriptorTable* dt);

#endif //MINI_FILE_SYSTEM_SRC_DESCRIPTORTABLE_DESCRIPTORTABLE_H_
