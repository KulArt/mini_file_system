//
// Created by Артур Кулапин on 11.03.2021.
//

#ifndef MINI_FILE_SYSTEM_SRC_BLOCKRECORD_BLOCKRECORD_H_
#define MINI_FILE_SYSTEM_SRC_BLOCKRECORD_BLOCKRECORD_H_

#include "../inode/inodes.h"
#include "../../utils/defines.h"

typedef struct BlockRecord {
  char name[DIR_NAME_LENGTH];
  int inode_parent_id;
} BlockRecord;

void InitBlockRecord(BlockRecord* block_record, int inode_id);
void DestroyBlockRecord(BlockRecord* block_record);



#endif //MINI_FILE_SYSTEM_SRC_BLOCKRECORD_BLOCKRECORD_H_
