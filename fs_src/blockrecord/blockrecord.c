//
// Created by Артур Кулапин on 11.03.2021.
//

#include "blockrecord.h"

void InitBlockRecord(BlockRecord* block_record, int inode_id) {
  block_record->inode_parent_id = inode_id;
}

void DestroyBlockRecord(BlockRecord* block_record) {
  block_record->inode_parent_id = -1;
  for (int i = 0; i < DIR_NAME_LENGTH; ++i) {
    block_record->name[i] = '\0';
  }
}

