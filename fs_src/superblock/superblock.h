//
// Created by Артур Кулапин on 11.03.2021.
//

#ifndef MINI_FILE_SYSTEM_SRC_SUPERBLOCK_SUPERBLOCK_H_
#define MINI_FILE_SYSTEM_SRC_SUPERBLOCK_SUPERBLOCK_H_

#include <stdint.h>
#include <stdbool.h>

#include "../../utils/defines.h"
#include "../../utils/utils.h"
#include "../inode/inodes.h"
#include "../../utils/defines.h"
#include "../descriptortable/descriptortable.h"


typedef struct SuperBlock {
  uint16_t inode_count;
  uint16_t block_size;
  uint16_t block_count;
  bool taken_blocks_mask[BLOCK_COUNT];
  bool taken_inodes_mask[INODE_COUNT];
  uint16_t taken_blocks_count;
} SuperBlock;

void InitSuperBlock(SuperBlock* super_block);
void DestroySuperBlock(SuperBlock* super_block);

ssize_t ReadSuperBlock(int fd, SuperBlock* super_block);
ssize_t WriteSuperBlock(int fd, SuperBlock* super_block);

ssize_t ReadInode(int fd, Inode* inode);
ssize_t WriteInode(int fd, Inode* inode);

void AddInode(int fd, SuperBlock* super_block);
void DestroyInode(SuperBlock* super_block, Inode* inode);

int GetBlockId(int fd, SuperBlock* super_block);

#endif //MINI_FILE_SYSTEM_SRC_SUPERBLOCK_SUPERBLOCK_H_
