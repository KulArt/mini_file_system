//
// Created by Артур Кулапин on 11.03.2021.
//

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include "superblock.h"

void InitSuperBlock(SuperBlock* super_block) {
  super_block->block_count = BLOCK_COUNT;
  super_block->inode_count = INODE_COUNT;
  super_block->block_size = BLOCK_SIZE;
  super_block->taken_blocks_count = 0;
  for (int i = 0; i < super_block->inode_count; ++i) {
    super_block->taken_inodes_mask[i] = false;
  }
  for (int i = 0; i < super_block->block_count; ++i) {
    super_block->taken_blocks_mask[i] = false;
  }
}

void DestroySuperBlock(SuperBlock* super_block) {
  super_block->block_count = 0;
  super_block->inode_count = 0;
  super_block->block_size = 0;
  super_block->taken_blocks_count = 0;
  for (int i = 0; i < super_block->inode_count; ++i) {
    super_block->taken_inodes_mask[i] = false;
  }
  for (int i = 0; i < super_block->block_count; ++i) {
    super_block->taken_inodes_mask[i] = false;
  }
}


ssize_t ReadSuperBlock(int fd, SuperBlock* super_block) {
  lseek(fd, 0, SEEK_SET);
  ssize_t total_read = ReadWhile(fd, (char*) super_block, sizeof(SuperBlock));
  if (total_read == -1) {
    fprintf(stderr, "%s, %s", strerror(errno), "can't read super block\n");
    exit(EXIT_FAILURE);
  }
  return total_read;
}

ssize_t WriteSuperBlock(int fd, SuperBlock* super_block) {
  lseek(fd, 0, SEEK_SET);
  ssize_t total_written =
      WriteWhile(fd, (const char*) super_block, sizeof(SuperBlock));
  if (total_written == -1) {
    fprintf(stderr, "%s: %s", strerror(errno), "can't write super block\n");
    exit(EXIT_FAILURE);
  }
  return total_written;
}


ssize_t ReadInode(int fd, Inode* inode) {
  lseek(fd, SUPERBLOCK_SIZE + DESCRIPTOR_TABLE_SIZE + sizeof(Inode) * inode->id, SEEK_SET);
  ssize_t read = ReadWhile(fd, (char*)(inode), sizeof(struct Inode));
  if (read == -1) {
    fprintf(stderr, "%s: %s %d", strerror(errno), "can't read inode with id: \n", inode->id);
    exit(EXIT_FAILURE);
  }
  return read;
}

ssize_t WriteInode(int fd, Inode* inode) {
  lseek(fd, SUPERBLOCK_SIZE + DESCRIPTOR_TABLE_SIZE + sizeof(Inode) * inode->id, SEEK_SET);
  ssize_t written = WriteWhile(fd, (char*)(inode), sizeof(struct Inode));
  if (written == -1) {
    fprintf(stderr, "%s: %s %d", strerror(errno), "can't write inode with id: \n", inode->id);
    return -1;
  }
  return written;
}

void AddInode(int fd, SuperBlock* super_block) {
  Inode* inode = (Inode*)calloc(1, sizeof(Inode));
  inode->id = FindFirstNonZero(super_block->taken_inodes_mask, INODE_COUNT);
  if (inode->id == -1) {
    free(inode);
    fprintf(stderr, "%s: %s", strerror(errno), "can't write in not free node\n");
    return;
  }
  ssize_t written = WriteInode(fd, inode);
  if (written == -1) {
    free(inode);
    fprintf(stderr, "%s: %s", strerror(errno), "can't create inode\n");
    exit(EXIT_FAILURE);
  }
}

void DestroyInode(SuperBlock* super_block, Inode* inode) {
  super_block->taken_inodes_mask[inode->id] = false;
}

int GetBlockId(int fd, SuperBlock* super_block) {
  int id = -1;
  for (int i = 0; i < super_block->block_count; ++i) {
    if (!super_block->taken_blocks_mask[i]) {
      return i;
    }
  }
  return -1;
}