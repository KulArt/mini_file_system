//
// Created by Артур Кулапин on 12.03.2021.
//

#ifndef MINI_FILE_SYSTEM_SRC_ACTIONS_ACTIONS_H_
#define MINI_FILE_SYSTEM_SRC_ACTIONS_ACTIONS_H_

#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>

#include "../../utils/defines.h"
#include "../fileutils/fileutils.h"
#include "../superblock/superblock.h"
#include "../block/block.h"
#include "../inode/inodes.h"
#include "../descriptortable/descriptortable.h"
#include "../stringhelper/stringhelper.h"

int InitFS(const char* path);
void DestroyFS(int fd);

void ls(int fd, const char* path, SuperBlock* sb);

void CreateDir(int fd, const char* path, SuperBlock* sb);
void CreateFile(int fd, const char* path, SuperBlock* sb);

void OpenFile(int fd, const char* path, SuperBlock* sb, DescriptorTable* dt);
void CloseFile(int fd, uint16_t fs_fd, SuperBlock* sb, DescriptorTable* dt);

void WriteToFile(int fd, uint16_t fs_fd, char* data,
                 uint32_t size, SuperBlock* sb, DescriptorTable* dt);
void WriteFromFile(int fd, uint16_t fs_fd, const char* path, SuperBlock* sb, DescriptorTable* dt);

void Lseek(int fd, uint16_t fs_fd, uint32_t pos, SuperBlock* sb, DescriptorTable* dt);

void ReadFromFile(int fd, uint16_t fs_fd, char* data,
                  uint32_t size, SuperBlock* sb, DescriptorTable* dt);

void ReadToFile(int fd, uint16_t fs_fd, const char* path,
                  uint32_t size, SuperBlock* sb, DescriptorTable* dt);

#endif //MINI_FILE_SYSTEM_SRC_ACTIONS_ACTIONS_H_
