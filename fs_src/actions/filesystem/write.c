//
// Created by Артур Кулапин on 12.03.2021.
//

#include "../actions.h"
#include <sys/stat.h>

void WriteToFile(int fd, uint16_t fs_fd, char* data,
                 uint32_t size, SuperBlock* sb, DescriptorTable* dt) {

  if (fd == -1) {
    fprintf(stderr, "Can't open file descriptor\n");
    exit(EXIT_FAILURE);
  }

  ReadSuperBlock(fd, sb);
  ReadDescriptorTable(fd, dt);

  Inode inode;
  inode.id = dt->inode_id[fs_fd];
  ReadInode(fd, &inode);

  uint32_t fd_position = dt->pos[fs_fd];

  if (!inode.is_file) {
    fprintf(stderr, "This path is to directory, not file\n");
    exit(EXIT_FAILURE);
  }

  if (!dt->fd_mask[fs_fd]) {
    fprintf(stderr, "File descriptor is closed\n");
    exit(EXIT_FAILURE);
  }

  uint32_t total_written = 0;
  uint32_t need_to_write_size = size;

  while (total_written != size) {
    uint16_t block_to_write_pos = (uint16_t) fd_position / BLOCK_SIZE;
    Block cur_block;
    cur_block.inode_parent_id = inode.id;
    if (block_to_write_pos < inode.taken_blocks_count) {
      if (ReadBlock(fd, &cur_block, inode.block_ids[block_to_write_pos], sb)
          == -1) {
        fprintf(stderr, "Can't read block\n");
        exit(EXIT_FAILURE);
      }
    } else {
      int new_block_id = GetBlockId(fd, sb);
      cur_block.id = new_block_id;
      if (new_block_id == -1) {
        fprintf(stderr, "Memory overflow, can't create extra block in fs\n");
        exit(EXIT_FAILURE);
      }
      if (inode.taken_blocks_count == NUM_BLOCK_IN_INODE) {
        fprintf(stderr,
                "Memory overflow, can't create extra block in this inode\n");
        exit(EXIT_FAILURE);
      }
      cur_block.data = (char *) calloc(BLOCK_SIZE, sizeof(char));
      memcpy(cur_block.data, data, BLOCK_SIZE);
      inode.block_ids[inode.taken_blocks_count] = new_block_id;
      inode.taken_blocks_count += 1;
      cur_block.data_size = BLOCK_SIZE;
      if (WriteInode(fd, &inode) == -1) {
        fprintf(stderr, "Can't write inode\n");
        exit(EXIT_FAILURE);
      }
      if (WriteBlock(fd, &cur_block, sb) == -1) {
        fprintf(stderr, "Can't write block\n");
        exit(EXIT_FAILURE);
      }
    }

    uint16_t position_in_block_data = (uint16_t) fd_position % BLOCK_SIZE;
    char* position_to_write = cur_block.data + position_in_block_data;
    uint32_t remain_size = BLOCK_SIZE - position_in_block_data;
    uint32_t size_to_write = need_to_write_size < remain_size ? need_to_write_size : remain_size;
    cur_block.data_size = position_in_block_data;
    memcpy(position_to_write, data, size_to_write);
    cur_block.data_size += size_to_write;

    if (WriteBlock(fd, &cur_block, sb) == -1) {
      fprintf(stderr, "Can't write block\n");
      exit(EXIT_FAILURE);
    }
    fd_position += size_to_write;
    data += size_to_write;
    total_written += size_to_write;
    need_to_write_size -= size_to_write;
  }

  dt->pos[fs_fd] = fd_position;
  WriteDescriptorTable(fd, dt);
  WriteInode(fd, &inode);
  WriteSuperBlock(fd, sb);

  printf("Total written: %d\n", total_written);
}

void WriteFromFile(int fd, uint16_t fs_fd, const char* path,
                   SuperBlock* sb, DescriptorTable* dt) {
  int real_fd = open(path, O_RDONLY);
  if (real_fd == -1) {
    fprintf(stderr, "Can't open file in real fs\n");
    exit(EXIT_FAILURE);
  }

  FILE* file = fdopen(fd, "rb");

  if (file == NULL) {
    fprintf(stderr, "File isn't opened");
    close(fd);
    exit(EXIT_FAILURE);
  }

  struct stat stat;
  if (fstat(fd, &stat) == -1) {
    fprintf(stderr, "Can't get statistics of file\n");
    fclose(file);
    close(fd);
    exit(EXIT_FAILURE);
  }
  fclose(file);
  close(fd);

  ssize_t size = stat.st_size;
  if (size == -1) {
    return;
  }

  real_fd = open(path, O_RDONLY);
  if (real_fd == -1) {
    fprintf(stderr, "Can't read file with data\n");
    return;
  }

  char* buffer = (char*) calloc(size, sizeof(char));
  size = ReadWhile(real_fd, buffer, size);

  close(real_fd);

  if (size == -1) {
    fprintf(stderr, "Can't read file with data. Abort!\n");
    return;
  }

  WriteToFile(fd, fs_fd, buffer, size, sb, dt);
}