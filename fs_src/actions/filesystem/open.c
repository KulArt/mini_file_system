#include "../actions.h"


void OpenFile(int fd, const char* path, SuperBlock* sb, DescriptorTable* dt) {
  if (fd == -1) {
    fprintf(stderr, "Can't open file descriptor\n");
    exit(EXIT_FAILURE);
  }

  ReadSuperBlock(fd, sb);
  ReadDescriptorTable(fd, dt);

  char path_to_traverse[256];
  char file_name[256];
  SplitPath(path, path_to_traverse, file_name);

  Inode inode;
  BuildInode(&inode, 0, true);
  TraversePath(fd, path_to_traverse, &inode, sb);

  if (inode.is_file) {
    fprintf(stderr, "This path is to file, not directory\n");
    exit(EXIT_FAILURE);
  }

  Block block;
  int block_id = inode.block_ids[0];
  block.data_size = 0;
  ReadBlock(fd, &block, block_id, sb);

  int desc = -1;

  for (int i = 0; i < block.records_count; ++i) {
    BlockRecord b = block.block_records[i];
    if (!strcmp(block.block_records[i].name, file_name)) {
      desc = BuildDescriptor(dt, block.block_records[i].inode_parent_id);
    }
  }

  if (desc == -1) {
    fprintf(stderr, "No any descriptors\n");
    exit(EXIT_FAILURE);
  } else {
    printf("Descriptor: %d\n", desc);
  }

  WriteSuperBlock(fd, sb);
  WriteDescriptorTable(fd, dt);

}

