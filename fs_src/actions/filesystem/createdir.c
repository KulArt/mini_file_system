//
// Created by Артур Кулапин on 12.03.2021.
//

#include "../actions.h"

void CreateDir(int fd, const char* path, SuperBlock* sb) {
  char path_to_traverse[256];
  char dir_name[256];
  SplitPath(path, path_to_traverse, dir_name);

  Inode inode;
  BuildInode(&inode, 0, false);
  TraversePath(fd, path_to_traverse, &inode, sb);

  Block block;
  ReadBlock(fd, &block, inode.block_ids[0], sb);

  uint16_t next_inode_id = CreateDirBlockAndInode(fd, false, inode.id, sb);

  BlockRecord record;
  DestroyBlockRecord(&record);
  block.block_records[0].inode_parent_id = next_inode_id;
  strcpy(block.block_records[block.records_count++].name, dir_name);

  ++inode.taken_blocks_count;

  WriteInode(fd, &inode);
  WriteBlock(fd, &block, sb);
  WriteSuperBlock(fd, sb);
}