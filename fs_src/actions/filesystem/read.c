//
// Created by Артур Кулапин on 12.03.2021.
//

#include "../actions.h"

void ReadFromFile(int fd, uint16_t fs_fd, char* data,
                     uint32_t size, SuperBlock* sb, DescriptorTable* dt) {

  ReadSuperBlock(fd, sb);
  ReadDescriptorTable(fd, dt);

  Inode inode;
  inode.id = dt->inode_id[fs_fd];
  ReadInode(fd, &inode);

  uint32_t fd_position = dt->pos[fs_fd];

  uint32_t total_read = 0;
  uint32_t need_to_read = size;

  while (total_read != size) {
    uint16_t block_to_read_pos = (uint16_t) fd_position / BLOCK_SIZE;
    if (block_to_read_pos > inode.taken_blocks_count) {
      break;
    }
    Block cur_block;
    cur_block.inode_parent_id = inode.id;
    cur_block.data_size = (size - total_read > BLOCK_SIZE ? BLOCK_SIZE : size - total_read);
    if (ReadBlock(fd, &cur_block, inode.block_ids[block_to_read_pos], sb) == -1) {
      fprintf(stderr, "Can't read block\n");
      exit(EXIT_FAILURE);
    }

    uint32_t position_in_block_data = (uint32_t) fd_position % BLOCK_SIZE;

    char* position_to_read = cur_block.data + position_in_block_data;
    uint32_t remain_read = cur_block.data_size - position_in_block_data;
    if (remain_read == 0) {
      break;
    }
    uint32_t size_to_read = need_to_read < remain_read ? need_to_read : remain_read;
    memcpy(data, position_to_read, size_to_read);
    fd_position += size_to_read;
    data += size_to_read;
    total_read += size_to_read;
    need_to_read -= size_to_read;
  }

  dt->pos[fs_fd] = fd_position;
  WriteDescriptorTable(fd, dt);
  data -= size;
  printf("Total read: %d, string: %s\n", total_read, data);
}

void ReadToFile(int fd, uint16_t fs_fd, const char* path,
                  uint32_t size, SuperBlock* sb, DescriptorTable* dt) {
  char* data = (char*)calloc(size, sizeof(char));
  ReadFromFile(fd, fs_fd, data, size, sb, dt);
  int real_fd = open(path, O_RDWR | O_CREAT);
  if (real_fd == -1) {
    fprintf(stderr, "Can't open file in real fs to write\n");
    exit(EXIT_FAILURE);
  }
  ssize_t written = WriteWhile(fd, data, size);
  free(data);
  close(real_fd);
  printf("Written %zd to %s\n", written, path);
}