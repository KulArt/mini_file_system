//
// Created by Артур Кулапин on 12.03.2021.
//

#include "../actions.h"

void CloseFile(int fd, uint16_t fs_fd, SuperBlock* sb, DescriptorTable* dt) {
  if (fd == -1) {
    fprintf(stderr, "Can't open file descriptor\n");
    exit(EXIT_FAILURE);
  }

  ReadSuperBlock(fd, sb);
  ReadDescriptorTable(fd, dt);

  FreeDescriptor(dt, fs_fd);
  printf("Descriptor %d is free\n", fs_fd);

  WriteDescriptorTable(fd, dt);
}
