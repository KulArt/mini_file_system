//
// Created by Артур Кулапин on 12.03.2021.
//

#include "../actions.h"

void DestroyFS(int fd) {
  if (fd == -1) {
    fprintf(stderr, "can't open descriptor to destroy FS\n");
    exit(EXIT_FAILURE);
  }
  ftruncate(fd, FS_SIZE);
  close(fd);
}