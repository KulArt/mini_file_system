//
// Created by Артур Кулапин on 12.03.2021.
//

#include "../actions.h"

void CreateFile(int fd, const char* path, SuperBlock* sb) {
  char path_to_traverse[256];
  char file_name[256];
  SplitPath(path, path_to_traverse, file_name);

  Inode inode;
  BuildInode(&inode, 0, true);
  TraversePath(fd, path_to_traverse, &inode, sb);

  Block block;
  block.data_size = 0;
  ReadBlock(fd, &block, inode.block_ids[0], sb);

  uint16_t next_inode_id = CreateDirBlockAndInode(fd, false, inode.id, sb);

  BlockRecord record;
  DestroyBlockRecord(&record);
  block.block_records[block.records_count++].inode_parent_id = next_inode_id;
  strcpy(block.block_records[block.records_count - 1].name, file_name);

  ++inode.taken_blocks_count;
  inode.id = next_inode_id;
  inode.taken_blocks_count = 0;
  inode.is_file = true;

  WriteInode(fd, &inode);
  WriteBlock(fd, &block, sb);
  WriteSuperBlock(fd, sb);
}