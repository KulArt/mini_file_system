//
// Created by Артур Кулапин on 12.03.2021.
//

#include "../actions.h"

void ls(int fd, const char* path, SuperBlock* sb) {

  Inode inode;
  TraversePath(fd, path, &inode, sb);

  Block block;
  ReadBlock(fd, &block, inode.block_ids[0], sb);

  for (int i = 0; i < block.records_count; ++i) {
    printf("%s", block.block_records[i].name);
    Inode file_inode;
    BuildInode(&file_inode, block.block_records[i].inode_parent_id, true);
    ReadInode(fd, &file_inode);
    printf("  ");
  }
  printf("\n");
}