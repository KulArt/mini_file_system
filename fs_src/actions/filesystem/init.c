//
// Created by Артур Кулапин on 12.03.2021.
//

#include "../actions.h"

int InitFS(const char* path) {
  int fd = open(path, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
  if (fd == -1) {
    fprintf(stderr, "can't open descriptor to init FS\n");
    exit(EXIT_FAILURE);
  }
  ftruncate(fd, FS_SIZE);

  SuperBlock sb;
  InitSuperBlock(&sb);
  WriteSuperBlock(fd, &sb);

  DescriptorTable descriptor_table;
  InitDescriptorTable(&descriptor_table);
  WriteDescriptorTable(fd, &descriptor_table);

  CreateDirBlockAndInode(fd, true, 0, &sb);
  return fd;
}