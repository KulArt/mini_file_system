//
// Created by Артур Кулапин on 12.03.2021.
//
#include "../actions.h"

void Lseek(int fd, uint16_t fs_fd, uint32_t pos, SuperBlock* sb, DescriptorTable* dt) {
  ReadSuperBlock(fd, sb);
  ReadDescriptorTable(fd, dt);
  if (!dt->fd_mask[fs_fd]) {
    fprintf(stderr, "Descriptor is closed\n");
    return;
  }
  if (pos >= FS_SIZE) {
    fprintf(stderr, "Position >= max_data_in_file. Abort!\n");
    exit(EXIT_FAILURE);
  }
  dt->pos[fs_fd] = pos;
  WriteDescriptorTable(fd, dt);
}
