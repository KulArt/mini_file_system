//
// Created by Артур Кулапин on 25.04.2021.
//

#include <stdio.h>
#include <stdlib.h>
#include "server.h"


int main(int argc, char** argv) {
  long port = 8000;
  if (argc == 1) {
    fprintf(stderr, "Using fs name and 8000 port\n");
    const char* fs_file_path = "fs";
    return ServerLoop(8000, fs_file_path);
  }

  if (argc == 2) {
    const char* fs_file_path = argv[1];
    fprintf(stderr, "Using 8000 port\n");
    return ServerLoop(8000, fs_file_path);
  }

  const char* fs_file_path = argv[1];
  port = strtol(argv[2], NULL, 10);
  return ServerLoop(port, fs_file_path);
}