//
// Created by Артур Кулапин on 24.04.2021.
//

#include "../fs_src/actions/actions.h"
#include "server.h"
#include "../utils//utils.h"
#include "../utils/net_utils.h"
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/epoll.h>



bool ProcessCommand(int socket, bool* is_inited, const char* path, int* fd,
                    SuperBlock* sb, DescriptorTable* dt) {
  enum Command command;
  if (!ReceiveCommand(socket, &command)) {
    return false;
  }


  switch (command) {
    case Exit: {
      DestroyFS(*fd);
      return false;
    }

    case Init: {
      if (!*is_inited) {
        *fd = InitFS(path);
        *is_inited = true;
      } else {
        char error_text[] = "FS is already inited\n";
        SendData(socket, error_text, strlen(error_text));
      }
      break;
    }

    case Mkdir: {
      if (path == NULL || path[0] != '/') {
        char error_text[] = "Incorrect path! It must starts with /\n";
        SendData(socket, error_text, strlen(error_text));
      } else {
        CreateDir(*fd, path, sb);
      }
      break;
    }

    case Ls: {
      char* cl_path = NULL;
      size_t path_len = 0;
      ReceiveData(socket, &cl_path, &path_len);
      if (cl_path == NULL) {
        char error_text[] = "Incorrect path! It must starts with /\n";
        SendData(socket, error_text, strlen(error_text));
      } else {
        ls(*fd, cl_path, sb);
      }
      break;
    }

    case Touch: {
      char* cl_path = NULL;
      size_t path_len = 0;
      ReceiveData(socket, &cl_path, &path_len);
      if (cl_path == NULL) {
        char error_text[] = "Incorrect path! It must starts with /\n";
        SendData(socket, error_text, strlen(error_text));
      } else {
        CreateFile(*fd, cl_path, sb);
      }
      break;
    }

    case Open: {
      char* cl_path = NULL;
      size_t path_len = 0;
      ReceiveData(socket, &cl_path, &path_len);
      if (cl_path == NULL) {
        char error_text[] = "Incorrect path! It must starts with /\n";
        SendData(socket, error_text, strlen(error_text));
      } else {
        OpenFile(*fd, cl_path, sb, dt);
      }
      break;
    }

    case Close: {
      char* cl_path = NULL;
      size_t path_len = 0;
      ReceiveData(socket, &cl_path, &path_len);
      if (cl_path == NULL) {
        char error_text[] = "Incorrect path! It must starts with /\n";
        SendData(socket, error_text, strlen(error_text));
      } else {
        CloseFile(*fd, atoi(cl_path), sb, dt);
      }
      break;
    }

    case Write: {
      if (path == NULL) {
        char error_text[] = "Incorrect path! It must starts with /\n";
        SendData(socket, error_text, strlen(error_text));
      } else {
        int desc = path[0] - '0';
        ssize_t size = strlen(path) - 2;
        char* data = (char*)calloc(size, sizeof(char));
        strncpy(data, path + 2, size);
        WriteToFile(*fd, desc, data, size, sb, dt);
      }
      break;
    }

    case Read: {
      if (path == NULL) {
        char error_text[] = "Incorrect path! It must starts with /\n";
        SendData(socket, error_text, strlen(error_text));
      } else {
        int desc = path[0] - '0';
        path += 2;
        ssize_t size = atoll(path);
        char *data = (char *) calloc(size, sizeof(char));
        ReadFromFile(*fd, desc, data, size, sb, dt);
      }
      break;
    }

    case LSeek: {
      if (path == NULL) {
        char error_text[] = "Incorrect path! It must starts with /\n";
        SendData(socket, error_text, strlen(error_text));
      } else {
        int desc = path[0] - '0';
        path += 2;
        Lseek(*fd, desc, atoll(path), sb, dt);
      }
      break;
    }

    default: {
      break;
    }

  }
  return true;
}

int ServerLoop(long port, const char* path) {
  SuperBlock sb;
  DescriptorTable dt;
  int fd = open(path, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
  if (fd == -1) {
    fprintf(stderr, "can't open descriptor to init FS\n");
    exit(EXIT_FAILURE);
  }
  bool* is_inited = (bool*)calloc(1, sizeof(bool));
  *is_inited = false;

  int sock = socket(AF_INET, SOCK_STREAM, 0);

  if (sock == -1) {
    fprintf(stderr, "Can't create socket\n");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_in addr;
  memset(&addr, 0, sizeof(struct sockaddr_in));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = htonl(INADDR_ANY);

  if (bind(sock, (struct sockaddr*) &addr, sizeof(addr)) == -1) {
    fprintf(stderr, "Can't create connection\n");
    exit(EXIT_FAILURE);
  }

  if (listen(sock, 1) == -1) {
    fprintf(stderr, "Can't listen\n");
    exit(EXIT_FAILURE);
  }

  int epoll_fd = epoll_create1(0);
  struct epoll_event event;
  memset(&event, 0, sizeof(struct epoll_event));
  event.events = EPOLLIN | EPOLLERR | EPOLLHUP;
  event.data.fd = sockd;
  epoll_ctl(epoll_fd, EPOLL_CTL_ADD, sockd, &event);

  while (true) {
    int connection_fd = -1;
    struct epoll_event event;
    memset(&event, 0, sizeof(struct epoll_event));
    int new_events = epoll_wait(epoll_fd, &event, 1, 1000);
    if (new_events <= 0) {
      continue;
    }
    connection_fd = accept(sockd, NULL, NULL);
    if (connection_fd == -1) {
        fprintf(stderr, "Connection error\n");
        exit(EXIT_FAILURE);
    }
    while (ProcessCommand(connection_fd, is_inited, path, &fd, &sb, &dt)) {
    }

    shutdown(connection_fd, SHUT_RDWR);
    close(connection_fd);
  }
  shutdown(sock, SHUT_RDWR);
  close(sock);
  close(epoll_fd);
}