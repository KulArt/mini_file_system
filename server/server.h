//
// Created by Артур Кулапин on 24.04.2021.
//

#ifndef MINI_FILE_SYSTEM_SERVER_SERVER_H_
#define MINI_FILE_SYSTEM_SERVER_SERVER_H_

int ServerLoop(long port, const char* path);

#endif //MINI_FILE_SYSTEM_SERVER_SERVER_H_
